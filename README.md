# NODE.JS Todo List App

Todo List based on node.js and mangodb. 

Project based on the tutorial of `The Net Ninja` on Youtube.

To try app, create your mondodb cluster and add the correct link to `mongoose.connect()` function. Then run from the root the command line `nodemon app.js`.

## MVC structure

### Model

Used for data
- TODOS
- USERS

### View

Used for template files (EJS)
- todo.ejs
- account.ejs
- etc.

### Controller

Controls the app sections
- todoController
- userController


## NoSQL DataBase

- Alternative to SQL Databases
- Store documents (JSON) in a database, isntead of tables with rows and columns
- Works really well with JavaScript (and therefore Node.JS)

***Storing JSON***
```json
[
    {
        Item: 'walk the dog'
    },
    {
        Item: 'eat some pie'
    }
]
```

## MongoDB - mongoose

First of all, create an account on mongodb.com. Then, create a new cluster and pick a free solution.

- Click on connect and add a user.
- Choose to connect with driver which will return you a link
- Use the function `mongoose.connect()` and pass in the mongodb link as parameter.

        mongodb+srv://<userName>:<userPassword>@<clusterName>-negtk.mongodb.net/test?retryWrites=true

### Create a mongo schema

```js
var todoSchema = new mongoose.Schema({
    item: String
});
```

Then, create a model type based on the Schema

```js
var name = mongoose.model('name', todoSchema);
```