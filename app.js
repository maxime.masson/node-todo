var express = require('express');
var todoController = require('./controllers/todoController');
var app = express();

// Setup template engine
app.set('view engine', 'ejs');

// Static files
app.use(express.static('./public'));

// Fire controllers which take the app param
todoController(app);

// Listen to port
app.listen(3000);
console.log("Hey you're listening to port 3000");